package filters;

import java.awt.*;

public class ExFilter extends Filter {
    
    public void defFilter(Color[][] image) {
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image[i].length; j++) {
                if (image[i][j].getRed() <= 128) {
                    image[i][j] = new Color((100 + image[i][j].getRed()) % 255,
                            image[i][j].getGreen(), image[i][j].getBlue());
                }
                if (image[i][j].getGreen() <= 128) {
                    image[i][j] = new Color(image[i][j].getRed(),
                            (100 + image[i][j].getGreen()) % 255, image[i][j].getBlue());
                }
                if (image[i][j].getBlue() <= 128) {
                    image[i][j] = new Color(image[i][j].getRed(),
                            image[i][j].getGreen(), (100 + image[i][j].getBlue()) % 255);
                }
                if (image[i][j].getRed() > 128) {
                    image[i][j] = new Color((image[i][j].getRed() - 100) % 255,
                            image[i][j].getGreen(), image[i][j].getBlue());
                }
                if (image[i][j].getGreen() > 128) {
                    image[i][j] = new Color(image[i][j].getRed(),
                            (image[i][j].getGreen() - 100) % 255, image[i][j].getBlue());
                }
                if (image[i][j].getBlue() > 128) {
                    image[i][j] = new Color(image[i][j].getRed(),
                            image[i][j].getGreen(), (image[i][j].getBlue() - 100) % 255);
                }
            }
        }
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image[i].length; j++) {
                if (image[i][j].getRed() < 128) {
                    image[i][j] = new Color(image[i][j].getRed() * 2 % 255,
                            image[i][j].getGreen(), image[i][j].getBlue());
                }
                if (image[i][j].getGreen() < 128) {
                    image[i][j] = new Color(image[i][j].getRed(),
                            image[i][j].getGreen() * 2 % 255, image[i][j].getBlue());
                }
                if (image[i][j].getBlue() < 128) {
                    image[i][j] = new Color(image[i][j].getRed(),
                            image[i][j].getGreen(), image[i][j].getBlue() * 2 % 255);
                }
            }
        }
    }
    
}
