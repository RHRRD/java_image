package filters;

import com.sun.imageio.plugins.jpeg.JPEGImageReader;
import com.sun.imageio.plugins.jpeg.JPEGImageReaderSpi;
import com.sun.imageio.plugins.jpeg.JPEGImageWriter;
import com.sun.imageio.plugins.jpeg.JPEGImageWriterSpi;

import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriter;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.FileImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class WorkWithImage {

    private static final String IMAGE_INPUT_FOLDER = "images/";
    private static final String IMAGE_OUTPUT_FOLDER = "imagefilters/res/";
    private static BufferedImage image;

    public static void main(String[] args) throws IOException {
        Filters bawFiler = new BawFilter();
        Filters blackoutFilter = new BlackoutFilter();
        Filters exFilter = new ExFilter();
        Filters oldFilter = new OldFilter();
        Filters negativeFilter = new NegativeFilter();
        Filters nothingFilter = new NothingFilter();

        WorkWithImage work = new WorkWithImage();


        blackoutFilter.setNext(exFilter);
        exFilter.setNext(negativeFilter);
        negativeFilter.setNext(oldFilter);
//        bawFiler.setNext(blackoutFilter);

        Color[][] tmp = work.getImageFromFile("test4.jpg");
        exFilter.useFilter(tmp);
        work.setAndSaveImageToFile("new_new_test4.jpg", tmp);
    }

    private Color[][] getImageFromFile(String fileName) throws IOException {
        ImageReaderSpi spi = new JPEGImageReaderSpi();
        ImageReader imageReader = new JPEGImageReader(spi);
        imageReader.setInput(new FileImageInputStream(new File(getClass().getClassLoader().getResource(IMAGE_INPUT_FOLDER + fileName).getFile())));
        ImageReadParam param = new ImageReadParam();
        image = imageReader.read(0, param);

        Color[][] colors = new Color[image.getHeight()][image.getWidth()];
        for (int i = 0; i < colors.length; i++) {
            for (int j = 0; j < colors[i].length; j++) {
                colors[i][j] = new Color(image.getRGB(j, i));
            }
        }
        return colors;
    }

    public void setAndSaveImageToFile(String fileName, Color[][] colors) throws IOException {
        setImage(colors);

        ImageWriter imageWriter = new JPEGImageWriter(new JPEGImageWriterSpi());
        new File(IMAGE_OUTPUT_FOLDER).mkdir();
        imageWriter.setOutput(new FileImageOutputStream(new File(IMAGE_OUTPUT_FOLDER + fileName)));
        imageWriter.write(image);
        ((FileImageOutputStream) imageWriter.getOutput()).close();
    }

    public void setImage(Color[][] colors) {
        for (int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight(); j++) {
                image.setRGB(i, j, colors[j][i].getRGB());
            }
        }
    }

}
