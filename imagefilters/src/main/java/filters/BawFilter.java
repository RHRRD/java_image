package filters;

import java.awt.*;

public class BawFilter extends Filter {
    
    public void defFilter(Color[][] image) {
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image[i].length; j++) {
                int temp = (image[i][j].getRed() + image[i][j].getGreen()
                        + image[i][j].getBlue()) / 3;
                image[i][j] = new Color(temp / 3, temp / 3, temp / 3);
            }
        }    
    }
    
}
