package filters;

import java.awt.Color;

public abstract class Filter implements Filters {

    private Filters next;

    public Filters setNext(Filters filters) {
        this.next = filters;
        return filters;
    }

    public void useFilter(Color[][] image) {
        defFilter(image);
        if (next != null) {
            next.useFilter(image);
        }
    }

    public abstract void defFilter(Color[][] image);

}
