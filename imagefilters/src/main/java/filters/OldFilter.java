package filters;

import java.awt.*;

public class OldFilter extends Filter {

    public void defFilter(Color[][] image) {
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image[i].length; j++) {
                image[i][j] = new Color(image[i][j].getRed() * 2 % 255,
                        image[i][j].getGreen() * 2 % 255, image[i][j].getBlue() * 2 % 255);
            }
        }
    }

}
