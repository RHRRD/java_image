package filters;

import java.awt.*;

public class NegativeFilter extends Filter {


    public void defFilter(Color[][] image) {
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image[i].length; j++) {
                image[i][j] = new Color(255 - image[i][j].getRed(),
                        255 - image[i][j].getGreen(), 255 - image[i][j].getBlue());
            }
        }
    }
}
