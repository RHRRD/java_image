package filters;

import java.awt.Color;

public interface Filters {

    public Filters setNext(Filters filters);
    public void useFilter(Color[][] image);

}
